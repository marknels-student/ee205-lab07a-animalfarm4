///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// A generic Node class.  May be used as a base class for a Singly Linked List
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   23_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once


class Node {
friend class SingleLinkedList;

protected:
	Node* next = nullptr;
		
}; // class Node
