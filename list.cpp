///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// A generic SingleLinkedList collection class.
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   23_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include "list.hpp"

const bool SingleLinkedList::empty() const {
	return (head == nullptr);
}


void SingleLinkedList::push_front( Node* newNode ) {
	if( newNode == nullptr )
		return;  // If newNode is nothing, then just quietly return
	
	newNode->next = head;
	head = newNode;
	
	count++;
}


Node* SingleLinkedList::pop_front() {
	if( head == nullptr )
		return nullptr; 

	Node* returnValue = head;
	
	head = head->next;
	
	count--;
	
	return returnValue;
}


Node* SingleLinkedList::get_first() const {
	return head;
}


Node* SingleLinkedList::get_next(const Node* currentNode) const {
	return currentNode->next;
}


unsigned int SingleLinkedList::size() const {
	return count;
}