///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file the.cpp
/// @version 3.0
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "node.hpp"
#include "list.hpp"

using namespace std;

int main() {
	cout << "I am Sam" << endl;

	Node node1;             // Instantiate a node
	SingleLinkedList list;  // Instantiate a SingleLinkedList
	
	cout << "empty() = " << list.empty() << "    ";
	cout << "size() = " << list.size() << endl;

	list.push_front( &node1 );

	cout << "empty() = " << list.empty() << "    ";
	cout << "size() = " << list.size() << endl;
	
	Node* node2 = list.pop_front();

	cout << "empty() = " << list.empty() << "    ";
	cout << "size() = " << list.size() << endl;


	list.push_front( new Node() );
	list.push_front( new Node() );
	list.push_front( new Node() );
	list.push_front( new Node() );

	cout << "empty() = " << list.empty() << "    ";
	cout << "size() = " << list.size() << endl;

	for( auto n = list.get_first() ; n != nullptr ; n = list.get_next( n )) {
		cout << "Node ptr = " << n << endl;
		list.pop_front();
	}

	cout << "empty() = " << list.empty() << "    ";
	cout << "size() = " << list.size() << endl;
}
