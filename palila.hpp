///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file palila.hpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "bird.hpp"

namespace animalfarm {

class Palila : public Bird {
public:
	Palila( std::string newWhereFound, enum Color newColor, enum Gender newGender );

	std::string whereFound;
	
	void printInfo();
};

} // namespace animalfarm

